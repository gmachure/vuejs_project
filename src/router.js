import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Lists from './views/Lists.vue'
import About from './views/About.vue'
import ShoppingList from './components/ShoppingList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/lists',
      name: 'lists',
      component: Lists
    },
    {
      path: '/lists/:id',
      name: 'shoppingList',
      component: ShoppingList
    },
    {
      path: '/about-us',
      name: 'about-us',
      component: About
    }
  ]
})
